import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { MatDividerModule } from '@angular/material/divider';
import { CadastroPacienteComponent } from './cadastro-paciente/cadastro-paciente.component';
import { ConfirmarMarcacaoComponent } from './confirmar-marcacao/confirmar-marcacao.component';
import { HorarioComponent } from './horario/horario.component';
import { LoginComponent } from './login/login.component';
import { NovaMarcacaoComponent } from './nova-marcacao/nova-marcacao.component';
import { ListaPacienteComponent } from './lista-paciente/lista-paciente.component';
import { ListaMarcacaoComponent } from './lista-marcacao/lista-marcacao.component';
import { DadosPacienteComponent } from './dados-paciente/dados-paciente.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MatDividerModule,
  ],
  declarations: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    CadastroPacienteComponent,
    ConfirmarMarcacaoComponent,
    HorarioComponent,
    LoginComponent,
    NovaMarcacaoComponent,
    ListaPacienteComponent,
    ListaMarcacaoComponent,
    DadosPacienteComponent
  ],
  exports: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent
  ]
})
export class ComponentsModule { }
