import { Component, OnInit } from '@angular/core';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/nova-marcacao', title: 'Nova Marcação',  icon: 'add', class: '' },
    { path: '/cadastro-paciente', title: 'Cadastrar Paciente',  icon: 'person_add', class: '' },
    { path: '/lista-paciente', title: 'Listagem de Pacientes',  icon: 'assignment_ind', class: '' },
    { path: '/lista-marcacao', title: 'Listagem de Marcações',  icon: 'assignment', class: '' },
    
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor() { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  
  isMobileMenu() {
      return $(window).width() <= 991;
  };
}
