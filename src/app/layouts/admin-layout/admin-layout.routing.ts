import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { CadastroPacienteComponent } from 'app/components/cadastro-paciente/cadastro-paciente.component';
import { ConfirmarMarcacaoComponent } from 'app/components/confirmar-marcacao/confirmar-marcacao.component';
import { HorarioComponent } from 'app/components/horario/horario.component';
import { LoginComponent } from 'app/components/login/login.component';
import { NovaMarcacaoComponent } from 'app/components/nova-marcacao/nova-marcacao.component';
import { ListaPacienteComponent } from 'app/components/lista-paciente/lista-paciente.component';
import { ListaMarcacaoComponent } from 'app/components/lista-marcacao/lista-marcacao.component';
import { DadosPacienteComponent } from 'app/components/dados-paciente/dados-paciente.component';

export const AdminLayoutRoutes: Routes = [
    // {
    //   path: '',
    //   children: [ {
    //     path: 'dashboard',
    //     component: DashboardComponent
    // }]}, {
    // path: '',
    // children: [ {
    //   path: 'userprofile',
    //   component: UserProfileComponent
    // }]
    // }, {
    //   path: '',
    //   children: [ {
    //     path: 'icons',
    //     component: IconsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'notifications',
    //         component: NotificationsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'maps',
    //         component: MapsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'typography',
    //         component: TypographyComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'upgrade',
    //         component: UpgradeComponent
    //     }]
    // }
    { path: 'dashboard', component: DashboardComponent },
    { path: 'user-profile', component: UserProfileComponent },
    { path: 'table-list', component: TableListComponent },
    { path: 'typography', component: TypographyComponent },
    { path: 'icons', component: IconsComponent },
    { path: 'maps', component: MapsComponent },
    { path: 'notifications', component: NotificationsComponent },
    { path: 'upgrade', component: UpgradeComponent },


    { path: 'cadastro-paciente', component: CadastroPacienteComponent },
    { path: 'confirmar-marcacao', component: ConfirmarMarcacaoComponent },
    { path: 'horario', component: HorarioComponent },
    { path: 'login', component: LoginComponent },
    { path: 'nova-marcacao', component: NovaMarcacaoComponent },
    { path: 'lista-paciente', component: ListaPacienteComponent },
    { path: 'lista-marcacao', component: ListaMarcacaoComponent },
    { path: 'dados-paciente', component: DadosPacienteComponent }
    
];
